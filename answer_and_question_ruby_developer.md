

# ¿Cómo utiliza Ruby on Rails el marco del Model View Controller (MVC)?

 El desarrollo web a menudo se puede dividir en tres subsistemas separados pero estrechamente integrados:
### Modelo (registro activo) :

manejado por la biblioteca Active Record, que forma el puente entre el código del programa Ruby y la base
de datos relacional.

### Vista (Vista de acción) : 
* La vista es la parte de la aplicación que ve el usuario final. En Rails, esto se
implementa mediante la biblioteca Action View, que se basa en Embedded Ruby (ERB) y determina cómo se
presentarán los datos.

### Controlador (controlador de acción) : 
* El controlador es como el agente de datos de una aplicación, manejando
la lógica que permite que el modelo y la vista se comuniquen entre sí. Esto se llama el controlador de acción
en Rails.

# Cuál es el propósito del rakefile disponible en el directorio de demostración en Ruby?

* El propósito de esta simple pregunta es asegurarse de que un desarrollador esté familiarizado con un desarrollo basado
en pruebas. Es posible que un principiante aún no haya tratado este archivo. El rakefile es similar al makefile en Unix
y ayuda a empaquetar y probar el código Rails. Es utilizado por la utilidad rake, que se envía de forma nativa con la 
instalación de Ruby.

# Explicar el rol de los subdirectorios app / controllers y app / helpers en Rails.

* El subdirectorio app / controllers contiene todas las clases de controlador para la aplicación. Los controladores
manejan las solicitudes web del usuario. El subdirectorio app / helpers contiene clases de ayuda, que se utilizan
para ayudar a las clases de modelo, vista y controlador. Al mantener las clases auxiliares en un subdirectorio
separado, las clases de modelo, vista y controlador pueden permanecer esbeltas y ordenadas.

# Cómo implementa Rails AJAX?
* Asynchronous JavaScript and XML (AJAX) es un conjunto de tecnologías utilizadas para recuperar datos de una página
web sin tener que actualizar la página en sí. Así es como los sitios web modernos pueden cultivar una experiencia
de usuario "de escritorio". El método Rails para implementar operaciones AJAX es corto y simple.

 * Primero, se dispara un gatillo. El desencadenante puede ser algo tan simple como que un usuario haga
 clic en un llamado a la acción.

* A continuación, el cliente web usa JavaScript para enviar datos a través de una solicitud XMLHttpRequest desde el
desencadenante a un controlador de acciones en el servidor.

* En el lado del servidor, una acción del controlador Rails recibe los datos y devuelve el fragmento HTML 
 correspondiente al cliente. El cliente recibe el fragmento y actualiza la vista en consecuencia.


# Qué es una migración de rieles?
*  Escriba un breve ejemplo de una simple migración de Rails con una tabla llamada clientes, una columna de cadena llamada nombre y una columna de texto llamada descripción.

* Al iniciar el comando $rake db:migrate  Migration table_name creará una migración de Rails.
Una migración de Rails se puede usar para crear, soltar o eliminar tablas y columnas. A continuación se proporciona una solución potencial.

```
 clase CreateCustomers <ActiveRecord :: Migración
 
  def up
    create_table: los clientes hacen | t |
      t.string: nombre
      t.text: descripción
 
     t.timestamps
    end
    end
 
    def down
     drop_table: clientes
   end
 end
```

# ¿Puede explicar la diferencia entre "HashWithIndifferentAccess" de ActiveSupport y "Hash" de Ruby?
* La clase "HashWithIndifferentAccess" tratará las claves de símbolos y las claves de cadena como equivalentes, mientras que la clase 
 "Hash" en Ruby utilizará la comparación más estricta = = en las claves; una clave de cadena equivalente no recuperará el valor de una
clave de símbolo determinada.


# Explain the role of garbage collection in Ruby on Rails.
 * Garbage collection frees up memory for other processes by removing pointer programs and inaccessible objects left behind after a program has executed. This frees the programmer from having to track objects created dynamically during runtime.
 
 
 
#  Compare Controller, Views, Helpers and Models in Ruby

| MVC        | README                                                                                                         |
| ------     | ------                                                                                                         |
| Controller | It is basically the subdirectory where the controller classes are located                                      |
| Views      | It generally hold the display templates which are important and are required to fill the data from application |
| Helpers    | It holds helper class that are useful and helpful in managing the other classes                                |
| Models     | It holds classes that generally model the data which is stored in the application database                     |


# Ruby diferencia entre Size Length y Count

* lengthes un método que no forma parte Enumerable, es parte de una clase concreta (como Stringo Array) y generalmente se ejecuta en tiempo O(1)(constante). Eso es lo más rápido posible, lo que significa que usarlo probablemente sea una buena idea.

Si debe usar lengtho sizees principalmente una cuestión de preferencia personal. Personalmente lo uso sizepara colecciones (hashes, matrices, etc.) y lengthpara cadenas, ya que para mí los objetos como los hashes y las pilas no tienen una longitud, sino un tamaño (definido en términos de los elementos que contienen). Por el contrario, es perfectamente normal suponer que un texto tiene cierta longitud. De todos modos, al final estás invocando el mismo método, por lo que la distinción semántica no es importante.

Enumerable#count, por otro lado, es una bestia totalmente diferente. Por lo general, debe usarse con un bloque o un argumento y devolverá el número de coincidencias en un Enumerable:

```
arr = [1, 1, 2, 3, 5, 6, 8]

arr.count(&:even?) # => 3
arr.count(1) # => 2

```
* Sin embargo, puede invocarlo sin ningún argumento y devolverá el tamaño del enumerable en el que se invocó:

```
arr.count # => 7
```

*Sin embargo, hay una implicación de rendimiento con esto: para calcular el tamaño de lo enumerable, el countmétodo lo atravesará, lo que no es particularmente rápido (especialmente para grandes colecciones). Algunas clases (como Array) implementan una versión optimizada de counten términos de length, pero muchas no lo hacen.

La conclusión para usted es que debe evitar usar el countmétodo si puede usar

#### length o size.

Una nota a los desarrolladores de Rails - ActiveRecord::Relation's length, sizey countmétodos tienen un significado totalmente diferente, pero eso es irrelevante para nuestra discusión actual.


# Metodos de Rails blank? o present? 


## Blank?
* En ocasiones hemos utilizado estos métodos sin saber dónde están la frontera de Rails y de Ruby. Tanto blank? como present? son métodos de Rails y los podemos utilizar en entornos de proyectos de Rails y dentro de una consola de Rails. Por ejemplo, cuando recuperamos un objeto de Active Record y queremos saber si tiene datos o no dependiendo de nuestros intereses de estética u otro tema, podemos utilizar cualquiera de estos métodos:

```
n = Notice.first

  Notice Load (10.6ms)  SELECT  “notices”.* FROM “notices”  ORDER BY “notices”.”id” ASC LIMIT 1

=> #<Notice id: 1, title: “Primera noticia”, created_at: “2015-11-29 19:45:08”, updated_at: “2015-11-29 19:45:34”>

2.2.1 :062 > n.present?

=> true

2.2.1 :063 > n.blank?

=> false`

```

Pero veamos cada uno de ellos por separado.

Blank?
Es un método de Rails – blank? y opera casi con cualquier objeto. La definición del método es:
```
def blank?
  respond_to?(:empty?) ? !!empty? : !self
end

```

Por ejemplo podemos comprobar que el resultado es true en los casos:

```
nil.blank? = true
[].blank? = true
{}.blank? = true
" ".blank? = true
"".blank? = true
```

Un recurso interesante: Cuando tengamos dudas sobre dónde se encuentra, podemos ejecutar el método source_location: por ejemplo

```
"".method(:blank?).source_location

=> ["/Users/carlossanchezperez/.rvm/gems/
ruby-2.2.1@accepts-nested-attributes-for/gems/
activesupport-4.2.0/lib/active_support/core_ext/
object/blank.rb", 116]


```

### Present?

Para el método present?  es lo contrario de blank? lo podemos comprobar en la definición de su método

```
# File activesupport/lib/active_support/core_ext/
  object/blank.rb, line 22

def present?
  !blank?
end
```

# Metodos de rails empty? o nil?

* Ahora veamos los métodos que podemos utilizar en Rails igualmente pero cuando abrimos una consola de irb de Ruby son métodos que sólo podemos utilizarlos en ese entorno. Veamos cada método de Ruby, empty? y nil?

### Empty?
* Este método sólo está accesible para algunos objetos como los objetos de tipo Hash, Array y String. Si en otros casos, por ejemplo, con un objeto Active Record intentamos preguntar por si el objeto recuperado es empty? nos dirá que :

```
NoMethodError: undefined method `empty?’ for #<Notice:0x007fa6747b6a70>

```

### Nil? 
* En Ruby, todas las clases heredan de la clase Object class. nil? que es un método de objeto; Por lo tanto, a menos que explícitamente sea ignorado, todas las clases tienen acceso a nil?

Por ejemplo si accedemos a nuestro objeto de Active Record y le preguntamos:

```
n = Notice.first

  Notice Load (10.6ms)  SELECT  “notices”.* FROM “notices”  ORDER BY “notices”.”id” ASC LIMIT 1

=> #<Notice id: 1, title: “Primera noticia”, created_at: “2015-11-29 19:45:08”, updated_at: “2015-11-29 19:45:34”>

```

* Podemos decir si el objeto resultante n es nil?. En este caso al obtener un objeto AR nos dirá FALSE, en caso contrario obtenemos TRUE, o si preguntamos si nil es nil? nos devuelve TRUE.

```
nil.nil?

=> true

```
### UN DETALLE INTERESANTE FINAL DE CONOCER
* Cuando estamos trabajando para consultar algo en nuestra Base de Datos con Active Record y necesitamos consultar un array con los datos:

```
n = Notice.where(title: [“Primera noticia”, “Segunda noticia”])

  Notice Load (0.8ms)  SELECT “notices”.* FROM “notices” WHERE “notices”.”title” IN (‘Primera noticia’, ‘Segunda noticia’)

=> #<ActiveRecord::Relation [#<Notice id: 1, title: “Primera noticia”, created_at: “2015-11-29 19:45:08”, updated_at: “2015-11-29 19:45:34”>, #<Notice id: 2, title: “Segunda noticia”, created_at: “2016-03-30 20:43:01”, updated_at: “2016-03-30 20:43:01”>]>

```

* Entonces hay diferencia en utilizar un método u otro:

```
2) n = Notice.where(title: [“Primera noticia”, “Segunda noticia”]).empty?

   (0.1ms)  SELECT COUNT(*) FROM “notices” WHERE “notices”.”title” IN (‘Primera noticia’, ‘Segunda noticia’)

=> false

2) n = Notice.where(title: [“Primera noticia”, “Segunda noticia”]).blank?

  Notice Load (0.2ms)  SELECT “notices”.* FROM “notices” WHERE “notices”.”title” IN (‘Primera noticia’, ‘Segunda noticia’)

=> false

```

* Esto quiere decir que es mejor utilizar el método empty? pero también obtenemos el mismo resultado con any? para el caso de nuestro ejemplo, sí es cierto pero queda claro que con empty? necesitamos únicamente consultar una única vez a la Base de Datos con un count, un único paso, en vez de consultar primero a la Base de Datos, como en el caso de blank? y verificar el array resultante los resultados obtenidos, es decir, necesitamos hacerlo en varios pasos con blank? y en un paso con empty?.

* Finamente si hacemos un benchmark sobre un ejemplo de utilización de empty?, present? y any? para un array de 500 mil elementos y de los que vamos a comprobar 100 mil veces en cada uno de los métodos del array, el proceso es:
```
Benchmark.bmbm do |x|

          x.report(“any with data:”)   { 100000.times { array.any? } }

          x.report(“present with data:”) { 100000.times { array.present? } }

          x.report(“empty with data:”) { 100000.times { array.empty? } }

end
```

* Resultados obtenidos:
```
                          user     system      total        real

any with data:       0.000000   0.000000   0.000000 (  0.007800)

present with data:   0.010000   0.000000   0.010000 (  0.012611)

empty with data:     0.000000   0.000000   0.000000 (  0.008894)

```

* En este caso any?, siendo un método de Ruby de Enumerable, se obtienen mejores resultados que utilizando empty? que se queda en segundo lugar, frente al peor resultado que se lo lleva present?.


# Modulos y Clases diferencias

* P: ¿Cuál es la diferencia entre una clase y un módulo?

R: Los módulos son colecciones de métodos(funciones) y contantes. No pueden generar instancias. Las clases pueden generar instancias (objetos) y cada instancia tiene un estado (las variables de instancia).

Los modulos se pueden mezclar en las clases y otros módulos. Los métodos y constantes del módulo que se mezcla se añaden a la clase, aumentando su funcionalidad. Sin embargo, las clases no se pueden mezclar con nada.

Una clase puede heredar de otra pero no de un módulo.

Un módulo no puede heredar.


# ¿Qué es un método singleton (solitario)?

* R: Un método singleton es una instancia de un método asignado a un objeto determinado.
Se crean incluyendo el objeto en la definición:

```
  class  Foo
	    end

	    foo = Foo.new
	    bar = Foo.new
	    def foo.hello
	      puts "Hola"
	    end
	    foo.hello
	    bar.hello
```

* Produce:

```
        Hola
	    prog.rb:10 undefined method 'hello' for #<Foo:0x4018d748> (NameError)
```


# Direfencia entre find, find_by, where

### Find
* find => Este registro único devuelto si la clave primaria (id) dada existe en el sistema; de lo contrario, dará un error.
  find=> esto se usa para buscar fila por id. Esto devolverá un solo registro.
  ejemplo
```
$rails console
irb(main)::19:0> User.find(1)
User Load (0.3ms)  SELECT "users".* FROM "users" WHERE "users"."id" = ? LIMIT ?  [["id", 1], ["LIMIT", 1]]
=> #<User id: 1, email: "fettx@yandex.com", created_at: "2020-04-27 23:02:14", updated_at: "2020-04-27 23:02:14", first_name: "Daniel", last_name: "Tinivella">
```
ahora si queremos buscar un User.id que no existe el ressultado de find es el siguiente 
```
irb(main)::19:0> User.find(10)   # este user no existe , veamos el resultado que arroja find
 User Load (0.2ms)  SELECT "users".* FROM "users" WHERE "users"."id" = ? LIMIT ?  [["id", 10], ["LIMIT", 1]]
Traceback (most recent call last):
        1: from (irb):20
ActiveRecord::RecordNotFound (Couldn't find User with 'id'=10)
```


## find_by

find_by => Esto devolverá un registro único depende del atributo dado, y si el valor del atributo no existe en la base de datos, devolverá nulo.
El nombre aquí es el atributo y debe existir en su Model.

irb(main)::19:0> User.find_by(1)
User Load (0.3ms)  SELECT "users".* FROM "users" WHERE "users"."id" = ? LIMIT ?  [["id", 1], ["LIMIT", 1]]
=> #<User id: 1, email: "fettx@yandex.com", created_at: "2020-04-27 23:02:14", updated_at: "2020-04-27 23:02:14", first_name: "Daniel", last_name: "Tinivella">
```
ahora si queremos buscar un User.id que no existe el ressultado de find_by es el siguiente 
```
irb(main)::19:0> User.find(10)   # este user no existe , veamos el resultado que arroja find
 User Load (0.2ms)  SELECT "users".* FROM "users" WHERE "users"."id" = ? LIMIT ?  [["id", 10], ["LIMIT", 1]]
Traceback (most recent call last):
        1: from (irb):20
ActiveRecord::RecordNotFound (Couldn't find User with 'id'=10)
```


where => Esto devolverá una relación de registro activa con cero o más registros que necesita usar primero para devolver solo un registro o cero en caso de que devuelva cero registros.
where => esto se utiliza para obtener registros activos en función de las condiciones para devolver la relación de registro activo (es decir, puede ser cero o más registros).

YourModel.where(:attrname => "something")
Address.where(:city => "Newyork")


Model.where(id: id_value).first


# Proc

* Un Proc objet es una encapsulación de un bloque de código, que se puede almacenar en una variable local, pasar a un método u otro Proc , y se puede llamar. Proc es un concepto esencial en Ruby y un núcleo de sus funciones de programación funcional.

```bash
square = Proc.new {|x| x**2 }

square.call(3)  #=> 9
# shorthands:
square.(3)      #=> 9
square[3]       #=> 9

```

# Proc and Lambda
```bash

# proc and lambda
# lambda checkea un numero de argumento mientras proc no lo hace

def lambda_return
  my_lam = lambda {return}
  my_lam.call
  puts "Hola mundo from lambda"
end

#lambda devuelve Hola mundo
lambda_return

def proc_return
  my_proc = Proc.new {return}
  my_proc.call
  puts "hola mundo from Proc"
end

proc_return
# proc no devuelva nada

```
### Active Record Callbacks
* 1 El ciclo de vida del objeto
Durante el funcionamiento normal de una aplicación Rails, se pueden crear, actualizar y destruir objetos. Active Record proporciona enlaces a este ciclo de vida del objeto para que pueda controlar su aplicación y sus datos.

Las devoluciones de llamada le permiten activar la lógica antes o después de una alteración del estado de un objeto.

* 2 Resumen de devoluciones de llamada
Las devoluciones de llamada son métodos que se llaman en determinados momentos del ciclo de vida de un objeto. Con las devoluciones de llamada es posible escribir código que se ejecutará cada vez que se cree, guarde, actualice, elimine, valide o cargue un objeto de registro activo desde la base de datos.

 * 2.1 Registro de devolución de llamada
Para utilizar las devoluciones de llamada disponibles, debe registrarlas. Puede implementar las devoluciones de llamada como métodos ordinarios y utilizar un método de clase de estilo macro para registrarlas como devoluciones de llamada:

```
  class User < ApplicationRecord
    validates :login, :email, presence: true
   
    before_validation :ensure_login_has_a_value
   
    private
      def ensure_login_has_a_value
	if login.nil?
	  self.login = email unless email.blank?
	end
      end
  end

```

* Los métodos de clase de estilo macro también pueden recibir un bloque. Considere usar este estilo si el código dentro de su bloque es tan corto que cabe en una sola línea:
```

class User < ApplicationRecord
  validates :login, :email, presence: true
 
  before_create do
    self.name = login.capitalize if name.blank?
  end
end


```
* Las devoluciones de llamada Callback también se pueden registrar para disparar solo en ciertos eventos del ciclo de vida:
```

class User < ApplicationRecord
  before_validation :normalize_name, on: :create
 
  # :on takes an array as well
  after_validation :set_location, on: [ :create, :update ]
 
  private
    def normalize_name
      self.name = name.downcase.titleize
    end
 
    def set_location
      self.location = LocationService.query(self)
    end
end

```


